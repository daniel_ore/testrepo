package p1;

public class Universidad {
    public String codigo;
    public String nombre_uni;
    public int año_fundacion;
    public String direccion;
    public Universidad(String codigo,String nombre_uni,int año_fundacion,String direccion){
        this.codigo=codigo;
        this.nombre_uni=nombre_uni;
        this.año_fundacion=año_fundacion;
        this.direccion=direccion;
    }
}
