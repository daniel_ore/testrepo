package p1;

public class Banco {
    public String razon_social;
    public String direccion;
    public float telefono;
    public String pagina_web;
    public Banco(String razon_social,String direccion,float telefono,String pagina_web){
        this.razon_social=razon_social;
        this.direccion=direccion;
        this.telefono=telefono;
        this.pagina_web=pagina_web;
    }    
}
