package p1;

public class Miembro {
    public float dni;
    public String nombre;
    public String apellido;
    public float ingresos;
    public String grado_instruccion;
    public boolean cuenta_bancaria;
    public boolean Jefe;
    public String banco;
    
    public Miembro(float dni,String nombre,String apellido,float ingresos, String grado_instruccion,char c,char d,String banco){
        this.dni=dni;
        this.nombre=nombre;
        this.apellido=apellido;
        this.ingresos=ingresos;
        this.grado_instruccion=grado_instruccion;
        this.cuenta_bancaria=c;
        this.Jefe=d;
        this.banco=banco;
    }
}
